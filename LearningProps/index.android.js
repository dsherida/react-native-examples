import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View
} from 'react-native';
import Clock from './Clock';

export default class LearningProps extends Component {
  
  state = {
    time: ''
  }

  componentDidMount() {
    setInterval(() => {
      this.setState({time: new Date().toLocaleString().split(' ')[4]});
    }, 1000);
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Clock time={this.state.time} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

AppRegistry.registerComponent('LearningProps', () => LearningProps);
