import React, { Component } from 'react';
import {
    Text
} from 'react-native';

export default class Clock extends Component {
    render() {
        return (
            <Text>{this.props.time}</Text>
        );
    }
}