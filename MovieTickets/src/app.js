import React, { Component } from 'react';
import { Navigator } from 'react-native-deprecated-custom-components';
import Movies from './Movies';
import Confirmation from './Confirmation';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { apiMiddleware, reducer } from './redux';

const store = createStore(reducer, {}, applyMiddleware(apiMiddleware));

if (module.hot) {
  // Enable Webpack hot module replacement for reducers
  module.hot.accept('./redux', () => {
    const nextRootReducer = require('./redux');
    store.replaceReducer(nextRootReducer);
  });
}

store.dispatch({type: 'GET_MOVIE_DATA'});

const RouteMapper = (route, navigator) => {
  if (route.name === 'movies') {
    return (
      <Movies navigator={navigator} />
    );
  } else if (route.name === 'confirmation') {
    return (
      <Confirmation code={route.code} navigator={navigator} />
    );
  }
};

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator
          // Default to movies route
          initialRoute={{ name: 'movies' }}
          // Use FloatFromBottom transition between screens
          configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromBottom}
          // Pass a route mapper functions
          renderScene={RouteMapper}
        />
      </Provider>
    );
  }
}