import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

class LearnignComponents extends Component {

  state = {
    counter: 1
  }

  text() {
    return <Text>That's an awesome component</Text>;
  }

  updateCounter() {
    this.setState({ counter: 2 });
  }

  render() {
    return (
      <View style={styles.container}>
        {this.text()}
        <Text numberOfLines={1}>
          The counter is {this.state.counter}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});

AppRegistry.registerComponent('LearnignComponents', () => LearnignComponents);