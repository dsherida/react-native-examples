import React, { Component } from 'react';
import { compose, createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { initialState, apiMiddleware, reducer } from './redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import Container from './container';
import { AsyncStorage } from 'react-native';
import { persistStore, autoRehydrate } from 'redux-persist';

const store = createStore(
    reducer,
    initialState,
    compose(
        applyMiddleware(
            thunk,
            logger
        ),
        autoRehydrate()
    )
);

// Begin periodically persisting the store
persistStore(store, { storage: AsyncStorage });

if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./redux', () => {
        const nextRootReducer = require('./redux');
        store.replaceReducer(nextRootReducer);
    });
}

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Container />
            </Provider>
        );
    }
}