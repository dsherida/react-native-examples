import React, { Component } from 'react';
import {
    Image,      // Renders background iamge
    StyleSheet, // CSS-like styles
    Text,       // Renders text
    TouchableOpacity,   // Handles row presses
    View        // Container component
} from 'react-native';
import Dimensions from 'Dimensions';

// Detect screen size to calculate row height
const screen = Dimensions.get('window');

export default class Row extends Component {

    // Extract movie and onPress props passed from List component
    render({ movie, onPress } = this.props) {
        // Extract values from movie object
        const { title, rating, image } = movie;
        
        return (
            // Row press handler
            <TouchableOpacity
                style={styles.row}
                onPress={onPress}
                activeOpacity={0.7}
            >
                {/* Background image */}
                <Image source={{uri: image}} style={styles.imageBackground}>
                    {/* Title */}
                    <Text style={[styles.text, styles.title]}>{title.toUpperCase()}</Text>
                    {/* Rating */}
                    <View style={styles.rating}>
                        {/* Icon */}
                        <Image
                            source={{uri: 'https://staticv2.rottentomatoes.com/static/images/icons/cf-lg.png'}}
                            style={styles.icon}
                        />
                        {/* Value */}
                        <Text style={[styles.text, styles.value]}>{rating}%</Text>
                    </View>
                </Image>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    row: {
        paddingBottom: 4,
    },
    imageBackground: {
        height: screen.height / 3,
        justifyContent: 'center',   // Center vertically
        alignItems: 'center',       // Center horizontally
    },
    text: {
        color: '#fff',
        backgroundColor: 'transparent',
        fontFamily: 'Avenir',
        fontWeight: 'bold',
        textShadowColor: '#222',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 4,
    },
    title: {
        fontSize: 22,
    },
    rating: {
        flexDirection: 'row',
    },
    icon: {
        width: 22,
        height: 22,
        marginRight: 5,
    },
    value: {
        fontSize: 16,
    },
});