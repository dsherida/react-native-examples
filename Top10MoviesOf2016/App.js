import React, { Component } from 'react';
import {
    StatusBar,  // Allows to hide the status bar
    Text
} from 'react-native';
import { Navigator }  from 'react-native-deprecated-custom-components';

import List from './List';
import Movie from './Movie';

// Define a route mapper function that will be responsible for 
// handling navigation between a list and a movie detail screens.
const RouteMapper = (route, navigationOperations, onComponentRef) => {
    if (route.name === 'list') {
        return (
            <List navigator={navigationOperations}></List>
        );
    } else if (route.name === 'movie') {
        return (
            <Movie
                movie={route.movie}
                navigator={navigationOperations}
            />
        );
    }
};

export default class App extends Component {
    componentDidMount() {
        // Hide status bar
        StatusBar.setHidden(true);
    }

    render() {
        return (
            // Handle navigation between screens
            <Navigator
                // Default to list route
                initialRoute={{name: 'list'}}
                // Use FloatFromBottom transition between screens
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromBottom}
                // Pass a route mapper functions
                renderScene={RouteMapper}
            />
        );
    }
}