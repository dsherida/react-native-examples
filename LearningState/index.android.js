import React, { Component } from 'react';
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native';

export default class LearningState extends Component {
  
  state = {
    happy: false
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Image 
          // Change an image depending on state
          source={{ uri: this.state.happy
            ? 'https://i.imgur.com/LNKc5V3.jpg' // happy
            : 'https://i.imgur.com/74KCGU8.jpg' // not happy
            }}
            style={{ width: 300, height: 300 }}
        />
        <TouchableHighlight
          underlayColor="#4FAD54"
          style={!this.state.happy
            ? styles.buttonWrapper
            : [styles.buttonWrapper, styles.buttonWrapperOff]}
          onPress={() => {
            // Change state value to the opposite of current one
            this.setState({ happy: !this.state.happy }); 
          }}
        >
          <Text style={styles.button}>
            {this.state.happy
              // Change a button text depending on state
              ? 'Take that bone away' // happy
              : 'Give her a bone' // not happy 
            }
          </Text>
        </TouchableHighlight>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  buttonWrapper: {
    backgroundColor: '#8BC051',
    borderRadius: 4,
    margin: 60,
    padding: 15,
  },
  buttonWrapperOff: {
    backgroundColor: '#cb0e40',
  },
  button: {
    color: '#FFFFFF',
    fontSize: 18,
  },
});

AppRegistry.registerComponent('LearningState', () => LearningState);
